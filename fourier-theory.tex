\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{Fourier Theory}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the Kleine AG on the \emph{Weil conjectures}, organized by Mingjia Zhang and Ben Heuer and taking place in the winter term 2021/2022 in Bonn.
    I thank both of the organizers for their effort and in particular Mingjia Zhang for helping me with some confusion.
    References for the material are \cite{freitag-kiehl} and \cite{kiehl-weissauer}.
    There are probably some mistakes in these notes (that are all my fault), so use them at your own risk!

    \vspace{2mm}

    The goal of our Kleine AG is to prove the following theorem.

    \begin{theorem}[Main Theorem, Deligne] \label{thm:deligne}
        Let $f \colon X \to S$ be a map of algebraic $\kappa$-schemes and let $\calF$ be a $\tau$-mixed sheaf on $X$.
        Then also the direct images with compact support $R^i f_! (\calF)$ are $\tau$-mixed and we have
        \[
            w(R^i f_! (\calF)) \leq w(\calF) + i.
        \]
    \end{theorem}

    We have already reduced this to a fairly concrete statement about certain sheaves on the affine line.
    The goal of this talk is to prove this statement (see \Cref{prop:main}) and thus finish the proof of the Main Theorem.

    \section{Notation}

    \begin{itemize}
        \item
        $n, m, m'$ will always denote positive integers with $n \mid m \mid m'$.
        $w$ will always denote a real number.

        \item
        We fix a finite field $\kappa$ of characteristic $p$ and cardinality $q$.
        Let $k$ be a fixed algebraic closure of $\kappa$ and denote the geometric Frobenius $a \mapsto a^{1/q}$ on $k$ by $\Fr$.
        Also let $\kappa_n$ be the unique degree $n$ extension of $\kappa$ inside $k$ and write $\Fr_n \coloneqq \Fr^n$.

        \item
        $X$ always denotes an algebraic $\kappa$-scheme (i.e.\ a $\kappa$-scheme that is separated and of finite type).
        We write $\abs{X}$ for the set of closed points of the underlying topological space of $X$.
        Given $x \in X$ we write $d(x) \coloneqq [\kappa(x) : \kappa]$ and $N(x) \coloneqq \abs{\kappa(x)} = q^{d(x)}$.

        \item
        Fix a prime number $\ell \neq p$ and an algebraic closure $\Qlbar$ of $\Ql$.
        Also fix an isomorphism $\tau \colon \Qlbar \to \CC$ of (abstract) fields.

        \item
        When we say \emph{sheaf} we really mean \emph{Weil-$\Qlbar$-sheaf}.
        We denote the derived category of sheaves on $X$ by $D^b_c(X)$.

        \item
        We identify sheaves on $\Spec(\kappa_n)$ with finite-dimensional $\Qlbar$-vector spaces with an automorphism $\Fr_n$.
        Given an abstract finite field extension $\kappa'$ of $\kappa$ of degree $n$ and a sheaf $\calF$ on $\Spec(\kappa')$ we can still obtain a $\Qlbar$-vector space with an automorphism $\Fr_n$ but it is only well-defined up to noncanonical isomorphism.
        Nevertheless we still obtain a well-defined characteristic polynomial $\det \roundbrvert[\big]{1 - t \Fr_n}{\calF}$.

        \item
        Given $K \in D^b_c(X)$ and $x \in X(\kappa_n)$ (resp.\ $x \in \abs{X}$) we write $K_x$ for the stalk of $K$ at $x$, i.e.\ the pullback of $K$ along $\Spec(\kappa_n) \to X$ (resp.\ $\Spec(\kappa(x)) \to X$).

        Similarly we write $R \Gamma(X, K)$ (resp.\ $R \Gamma_c(X, K)$) for the derived pushforward (with compact support) of $K$ along $X \to \Spec(\kappa)$ and $H^i(X, K)$ (resp.\ $H^i_c(X, K)$) for its cohomology.

        \item
        For $b \in \Qlbar^{\times}$ we denote by $\calL_b$ the lisse sheaf of rank $1$ on $\Spec(\kappa)$ with associated character $\Fr \mapsto b$.

        \item
        We write $\norm{\blank}$ for the standard norm on the space of $\CC$-valued functions on some finite set.
    \end{itemize}

    \section{\texorpdfstring{Generalities on $\ell$-adic cohomology}{Generalities on l-adic cohomology}}

    \begin{theorem}[Base Change] \label{thm:base-change}
        Let
        \[
        \begin{tikzcd}
            X' \arrow{r}{g'} \arrow{d}{f'}
            & X \arrow{d}{f}
            \\
            S' \arrow{r}{g}
            & S
        \end{tikzcd}
        \]
        be a pullback diagram of algebraic $\kappa$-schemes and let $K \in D^b_c(X)$.
        Then there is a natural isomorphism
        \[
            g^* Rf_!(K) \cong Rf'_! g'^*(K).
        \]
    \end{theorem}

    \begin{theorem}[Projection Formula] \label{thm:proj-formula}
        Let $f \colon X \to S$ be a map of algebraic $\kappa$-schemes and let $K \in D^b_c(S)$ and $L \in D^b_c(X)$.
        Then there is a natural isomorphism
        \[
            K \otimes^L R f_!(L) \cong R f_! \roundbr[\big]{f^*(K) \otimes^L L}.
        \]
    \end{theorem}

    \begin{theorem}[Grothendieck Trace Formula] \label{thm:trace-formula}
        Let $K \in D^b_c(X)$.
        Then the following identities hold:
        \begin{align*}
            \prod_{i \in \ZZ} \prod_{x \in \abs{X}} \det \roundbrvert[\big]{1 - t^{d(x)} \Fr_{d(x)}}{\scrH^i(K)_x}^{(-1)^i} &= \prod_{i \in \ZZ} \det \roundbrvert[\big]{1 - t \Fr}{H^i_c(X, K)}^{(-1)^i} \\
            \sum_{i \in \ZZ} \sum_{x \in X(\kappa_n)} (-1)^i \tr \roundbrvert[\big]{\Fr_n}{\scrH^i(K)_x} &= \sum_{i \in \ZZ} (-1)^i \tr \roundbrvert[\big]{\Fr_n}{H^i_c(X, K)}
        \end{align*}
    \end{theorem}

    \begin{theorem}[Poincaré Duality] \label{thm:poincare}
        Suppose that $X$ is smooth and let $K \in D^b_c(X)$.
        Then there is a natural isomorphism
        \[
            R \Gamma(X, K^\vee)(d)[2d] \cong R \Gamma_c(X, K)^{\vee}.
        \]
    \end{theorem}

    \section{Pure, Mixed and Real Sheaves}

    \begin{definition}
        Let $\calG$ be a sheaf on $X$.
        \begin{itemize}
            \item
            $\calG$ is \emph{$\tau$-pure of weight $w$} if we have $\abs{\tau(\alpha)}^2 = N(x)^w$ for all $x \in \abs{X}$ and eigenvalues $\alpha \in \Qlbar^{\times}$ of $\Fr_{d(x)} \colon \calG_x \to \calG_x$.

            \item
            $\calG$ is \emph{$\tau$-mixed} if there exists a filtration of $\calG$ whose graded pieces are $\tau$-pure (possibly of varying weight).

            \item
            $\calG$ is \emph{$\tau$-real} if the characteristic polynomial $\tau \det \roundbrvert[\big]{1 - t \Fr_{d(x)}}{\calG_x} \in \CC[t]$ has coefficients in $\RR$ for all $x \in \abs{X}$.

            \item
            We define
            \[
                w(\calG) \coloneqq \sup_{x \in \abs{X}} \max_{\alpha} \log_{N(x)}\roundbr[\big]{\abs{\tau(\alpha)}^2} \in \RR
            \]
            where $\alpha$ runs through all eigenvalues of $\Fr_{d(x)} \colon \calG_x \to \calG_x$.

            \item
            We define functions $f^{\calG}_n \colon X(\kappa_n) \to \CC$ (depending on $n$) by setting $f^{\calG}_n(x) \coloneqq \tau \tr \roundbrvert{\Fr_n}{\calG_x}$.

            \item
            We define
            \[
                \norm{\calG} \coloneqq \sup \set[\Big]{\rho \in \RR}{ \limsup_n q^{-n(\rho + \dim(X))} \cdot \norm[\big]{f^{\calG}_n}^2 > 0 } \in \RR.
            \]
        \end{itemize}
        We also extend the definition of $f^{\calG}_n$ to objects $K \in D^b_c(X)$ by setting $f^K_n \coloneqq \sum_{i \in \ZZ} (-1)^i f^{\scrH^i(K)}_n$.
    \end{definition}

    \begin{theorem} \label{thm:norm-w-curves}
        Suppose that $X$ is smooth of pure dimension $1$ and let $\calG$ be a $\tau$-mixed sheaf on $X$ that does not admit sections with finite support.
        Then we have $\norm{\calG} = w(\calG)$.
    \end{theorem}

    \begin{theorem} \label{thm:real-mixed}
        Let $\calG$ be a $\tau$-real sheaf on $X$.
        Then $\calG$ is $\tau$-mixed.
    \end{theorem}

    The following lemma doesn't really belong here but I don't know where else to put it.

    \begin{lemma} \label{lem:lin-alg}
        Let $V$ be a finite-dimensional $\CC$-vector space and let $\varphi \colon V \to V$ be a ($\CC$-linear) automorphism all of whose eigenvalues $\alpha \in \CC^{\times}$ satisfy $\abs{\alpha}^2 = u$ for some fixed $u \in \RR_{>0}$.
        Then the the characteristic polynomial of the automorphism $\varphi \oplus (u \cdot \varphi^{-1}) \colon V \oplus V \to V \oplus V$ has real coefficients.
    \end{lemma}

    \section{\texorpdfstring{The $\ell$-adic Fourier transform (after Laumon)}{The l-adic Fourier transform (after Laumon)}}

    \subsection{The Fourier Transform for Finite Abelian Groups} \label{subsec:class-fourier}

    Let us first recall the Fourier transform for finite Abelian groups.
    Let $G$ be a finite Abelian group and let $\psi \colon G \times G \to \CC^{\times}$ be a symmetric nondegenerate pairing.

    \begin{definition}\label{def:class-fourier}
        Given a function $f \colon G \to \CC$ we define its \emph{Fourier transform} $T_{\psi} f \colon G \to \CC$ by the formula
        \[
            (T_{\psi} f)(h) \coloneqq \sum_{g \in G} f(g) \cdot \psi(g, h)^{-1}.
        \]
    \end{definition}

    The Fourier transformation has the following properties:

    \begin{lemma}
        Let $f \colon G \to \CC$ be a function on $G$.
        \begin{itemize}
            \item[(Fourier Inversion):]
            We have $\roundbr[\big]{T_{\psi^{-1}} \circ T_{\psi}} (f) = \abs{G} \cdot f$.
            
            \item[(Plancherel Formula):]
            We have $\norm{T_{\psi} f} = \abs{G}^{1/2} \cdot \norm{f}$.
        \end{itemize}
    \end{lemma}

    In the following we will be interested in the finite abelian groups $\kappa_n$.

    \begin{notation}
        We introduce the following notation (where $\psi$ denotes a character of $\kappa_n$).
        \begin{itemize}
            \item
            We set $\psi_{\kappa_m} \coloneqq \psi \Tr_{\kappa_m/\kappa_n}$.
            This is now a character of $\kappa_m$.

            \item
            Given $b \in \kappa_n$ we write $\psi_b$ for the character $\kappa_n \ni a \mapsto \psi(ba)$.
        \end{itemize}
        These constructions are compatible in the sense that $\psi_{\kappa_m, b} = \psi_{b, \kappa_m}$.

        Now given a nontrivial character $\psi$ of $\kappa$ we have an associated symmetric nondegenerate pairing on $\kappa_n$ that is given by
        \[
            (a, b) \mapsto \psi\roundbr[\big]{\Tr_{\kappa_n/\kappa}(ab)} = \psi_{\kappa_n, b}(a).
        \]
        We denote this pairing again by $\psi$.
    \end{notation}

    \subsection{Artin-Schreier Coverings}

    \begin{definition}
        We define the \emph{$n$-th Artin-Schreier map}
        \[
            \wp^{(n)} \colon \affline_{\kappa} \to \affline_{\kappa}, \qquad x \mapsto x^{q^n} - x.
        \]
        We also define the maps
        \[
            \alpha^{(m, n)} \colon \affline_{\kappa} \to \affline_{\kappa}, \qquad x \mapsto \sum_{i = 0}^{m/n - 1} x^{q^{in}}.
        \]
        One immediately checks the identity $\wp^{(n)} \alpha^{(m, n)} = \wp^{(m)}$.
    \end{definition}

    \begin{lemma}
        $\wp^{(n)}$ is a finite étale geometrically connected covering of degree $q^n$.
        After extending scalars to $\kappa_n$ it is Galois (even Abelian) with automorphism group
        \[
            \kappa_n \cong \Aut(\wp^{(n)}_{\kappa_n}), \qquad a \mapsto (x \mapsto x + a).
        \]
        Under this isomorphism the map $\Aut(\wp^{(m)}_{\kappa_m}) \to \Aut(\wp^{(n)}_{\kappa_m})$ induced by $\alpha^{(m, n)}$ identifies with $\Tr_{\kappa_m/\kappa_n} \colon \kappa_m \to \kappa_n$.
    \end{lemma}

    \begin{notation}
        Given a character $\psi \colon \kappa_n \to \Qlbar^{\times}$ we denote by $\calL(\psi)$ the associated lisse sheaf of rank $1$ on $\affline_{\kappa_n}$.
        Note that we have $\calL(\psi_{\kappa_m}) \cong \calL(\psi)_{\kappa_m}$.
    \end{notation}

    \begin{lemma}[Cohomology and Stalks of $\calL(\psi)$] \label{lem:Lpsi-stalks}
        Let $\psi \colon \kappa_n \to \Qlbar^{\times}$ be a character.
        Then we have
        \[
            H^i_c(\affline_{\kappa_n}, \calL(\psi)) \cong
            \begin{cases}
                \Qlbar(-1) & \text{if $\psi = 1$ and $i = 2$,} \\
                0 & \text{else.}
            \end{cases}
        \]
        Moreover, for $a \in \affline_{\kappa_n}(\kappa_m) = \kappa_m$ we have
        \[
            \det \roundbrvert[\big]{\Fr_m}{\calL(\psi)_a} = \psi_{\kappa_m}(a)^{-1}.
        \]
    \end{lemma}

    \begin{proof}
        It is a standard result in étale cohomology that $R \Gamma(\affline_{\kappa_n}, \Qlbar) \cong \Qlbar$.
        Applying \Cref{thm:poincare} to this yields $R \Gamma_c(\affline_{\kappa_n}, \Qlbar) \cong \Qlbar(-1)[-2]$.
        The first part of the lemma now follows because $R \wp^{(n)}_{\kappa_n, *}(\Qlbar) \cong \bigoplus_{\psi} \calL(\psi)$ (where the direct sum is over all characters of $\kappa_n$).

        Let us now turn to the second part and suppose we are given $a \in \affline_{\kappa_n}(\kappa_m) = \kappa_m$.
        Then we can choose $b \in \affline_{\kappa_n}(\kappa_{m'})$ for some $m'$ such that $\wp^{(n)}_{\kappa_n}(b) = a$.
        This gives rise to a commutative diagram
        \[
        \begin{tikzcd}
            \Spec(\kappa_{m'}) \arrow{r}{b} \arrow{d}
            & \affline_{\kappa_n} \arrow{d}{\wp^{(n)}_{\kappa_n}}
            \\
            \Spec(\kappa_m) \arrow{r}{a}
            & \affline_{\kappa_n}.
        \end{tikzcd}
        \]
        The induced map $\Gal(\kappa_{m'}/\kappa_m) \to \Aut(\wp^{(n)}_{\kappa_n})$ sends $\Fr_m$ to the automorphism $x \mapsto x - \Tr_{\kappa_m/\kappa_n}(a)$, i.e.\ the element $- \Tr_{\kappa_m/\kappa_n}(a) \in \kappa_n$.
        The result now readily follows as $\psi(-\Tr_{\kappa_m/\kappa_n}(a)) = \psi_{\kappa_m}(a)^{-1}$.
    \end{proof}

    \subsection{\texorpdfstring{The $\ell$-adic Fourier transform}{The l-adic Fourier transform}}

    In this subsection $\psi$ always denotes a nontrivial character $\kappa \to \Qlbar^{\times}$.

    \begin{definition}[Fourier Transform] \label{def:l-adic-fourier}
        Define the \emph{$\ell$-adic Fourier Transform} $T_{\psi} \colon D^b_c(\affline_{\kappa}) \to D^b_c(\affline_{\kappa})$ by the formula
        \[
            T_{\psi}(K) \coloneqq R \pr_{1, !} \roundbr[\big]{\pr_2^* K \otimes m^* \calL(\psi)}[1].
        \]
        Here $m \colon \affspace^2_{\kappa} \to \affline_{\kappa}$ denotes the multiplication map.
    \end{definition}

    \begin{lemma}[Stalks of the Fourier Transform] \label{lem:fourier-stalks}
        For $K \in D^b_c(\affline_{\kappa})$ and $b \in \affline_{\kappa}(\kappa_n) = \kappa_n$ we have
        \[
            T_{\psi}(K)_b \cong R \Gamma_c \roundbr[\big]{\affline_{\kappa_n}, K_{\kappa_n} \otimes \calL(\psi_{\kappa_n, b})}[1]
        \]
        in $D^b_c(\Spec(\kappa_n))$.
    \end{lemma}

    \begin{proof}
        Applying \Cref{thm:base-change} yields
        \[
            T_{\psi}(K)_b \cong R \Gamma_c \roundbr[\big]{\affline_{\kappa_n}, K_{\kappa_n} \otimes \lambda^{b, *} \calL(\psi)_{\kappa_n}}[1]
        \]
        where the map $\lambda^b \colon \affline_{\kappa_n} \to \affline_{\kappa_n}$ is given by $x \mapsto bx$.
        Thus we are done if we can show that $\lambda^{b, *} \calL(\psi)_{\kappa_n} \cong \calL(\psi_{\kappa_n, b})$.
        To see this note that we have a commutative diagram
        \[
        \begin{tikzcd}
            \affline_{\kappa_n} \arrow{r}{\lambda^b} \arrow{d}{\wp^{(n)}_{\kappa_n}}
            & \affline_{\kappa_n} \arrow{d}{\wp^{(n)}_{\kappa_n}}
            \\
            \affline_{\kappa_n} \arrow{r}{\lambda^b}
            & \affline_{\kappa_n}
        \end{tikzcd}
        \]
        inducing the map $\Aut(\wp^{(n)}_{\kappa_n}) \to \Aut(\wp^{(n)}_{\kappa_n})$ that is given by $a \mapsto ba$ under the isomorphism $\kappa_n \cong \Aut(\wp^{(n)}_{\kappa_n})$.
    \end{proof}

    \begin{remark}
        Let's try to explain why $T_{\psi}$ is called a \enquote{Fourier transform} by giving the following (incomplete) table of analogies (compare with \Cref{subsec:class-fourier}).
        \begin{center}
        \begin{tabular}{c | c}
            $G$
            & $\affline_{\kappa}$
            \\[0.5ex]
            $f \colon G^n \to \CC$
            & $K \in D^b_c(\affspace^n_{\kappa})$
            \\[0.5ex]
            $\psi^{-1}$
            & $m^* \calL(\psi)$
            \\[0.5ex]
            $\sum_{h \in G} f(g, h)$
            & $R \pr_{1,!}(K)$
        \end{tabular}
        \end{center}
        Using this table one should be able to compare \Cref{def:l-adic-fourier} with \Cref{def:class-fourier} (except for possibly the appearing shift).
    \end{remark}

    \begin{lemma} \label{lem:plancherel}
        Let $K \in D^b_c(\affline_{\kappa})$.
        Then we have
        \[
            f^{T_{\psi} K}_n = - T_{\tau \psi}(f^K_n).
        \]
        In particular we get a Plancherel Formula $\norm{f^{T_{\psi} K}_n} = q^{n/2} \cdot \norm{f^K_n}$.
    \end{lemma}

    \begin{proof}
        We make the following computation (for $b \in \affline_{\kappa}(\kappa_n) = \kappa_n$).
        \begin{align*}
            f^{T_{\psi} K}_n(b) & = \tau \sum_{i \in \ZZ} (-1)^i \tr \roundbrvert[\big]{\Fr_n}{\scrH^i(T_{\psi}K)_b} \\
            & \stackrel{\mathclap{\ref{lem:fourier-stalks}}}{=} - \tau \sum_{i \in \ZZ} (-1)^i \tr \roundbrvert[\big]{\Fr_n}{H^i_c(\affline_{\kappa_n}, K_{\kappa_n} \otimes \calL(\psi_{\kappa_n, b}))} \\
            & \stackrel{\mathclap{\ref{thm:trace-formula}}}{=} - \tau \sum_{i \in \ZZ} \sum_{a \in \kappa_n} (-1)^i \tr \roundbrvert[\big]{\Fr_n}{\scrH^i(K_{\kappa_n})_a \otimes \calL(\psi_{\kappa_n, b})_a} \\
            &= - \tau \sum_{a \in \kappa_n} \sum_{i \in \ZZ} (-1)^i \tr \roundbrvert[\big]{\Fr_n}{\scrH^i(K)_a} \cdot \det \roundbrvert[\big]{\Fr_n}{\calL(\psi_{\kappa_n, b})_a} \\
            & \stackrel{\mathclap{\ref{lem:Lpsi-stalks}}}{=} - \sum_{a \in \kappa_n} f^K_n(a) \cdot \tau \psi_{\kappa_n}(ab)^{-1} \\
            & = - T_{\tau \psi}(f^K_n)(b) \qedhere
        \end{align*}
    \end{proof}
    
    We now also want to establish a \enquote{Fourier Inversion}-result for the $\ell$-adic Fourier Transform.
    To do so, we need the following key computation that should be compared to the identity
    \[
        \sum_{h \in G} \psi(h, k)^{-1} \cdot \psi(g, h) =
        \begin{cases}
            \abs{G} & \text{if $g = k$,} \\
            0 & \text{else.}
        \end{cases}
    \]

    \begin{lemma} \label{lem:key-comp}
        We have
        \[
            R \pr_{13, !} \roundbr[\big]{\pr_{23}^* m^* \calL(\psi) \otimes \pr_{12}^* m^* \calL(\psi^{-1})} \cong \Delta_*(\Qlbar)(-1)[-2]
        \]
        where $\Delta \colon \affline_{\kappa} \to \affspace^2_{\kappa}$ denotes the diagonal map.
    \end{lemma}

    \begin{proof}
        We first claim that we have
        \[
            \pr_{23}^* m^* \calL(\psi) \otimes \pr_{12}^* m^* \calL(\psi^{-1}) \cong \alpha^* m^* \calL(\psi)
        \]
        where $\alpha \colon \affspace^3_{\kappa} \to \affspace^2_{\kappa}$ is the map $(x, y, z) \mapsto (y, z - x)$.
        To see this we consider
        \[
            X \coloneqq \Spec \roundbr[\big]{\kappa[x, y, z][\varepsilon, \eta]/(\varepsilon^q - \varepsilon - xy, \eta^q - \eta - yz)} \xrightarrow{\chi} \affspace^3_{\kappa}.
        \]
        $\chi$ is a finite étale geometrically connected Abelian covering with automorphism group
        \[
            \kappa \times \kappa \cong \Aut(\chi), \qquad (a_{\varepsilon}, a_{\eta}) \mapsto (\varepsilon \mapsto \varepsilon + a_{\varepsilon}, \eta \mapsto \eta + a_{\eta}).
        \]
        Moreover we have commutative diagrams
        \[
        \begin{tikzcd}
            X \arrow{r}{\textbf{(??)}} \arrow{d}{\chi}
            & \affline_{\kappa} \arrow{d}{\wp^{(1)}}
            \\
            \affspace^3_{\kappa} \arrow{r}{\textbf{(?)}}
            & \affline_{\kappa}
        \end{tikzcd}
        \]
        with the maps $\textbf{(?)}$ and $\textbf{(??)}$ given by $m \circ \pr_{23}$, $m \circ \pr_{12}$, $m \circ \alpha$ and $(x, y, z, \varepsilon, \eta) \mapsto \eta, \varepsilon, \eta - \varepsilon$ respectively.
        Hence we obtain induced maps on the automorphism groups that are given by
        \[
            \kappa \times \kappa \to \kappa, \qquad (a_{\varepsilon}, a_{\eta}) \mapsto a_{\eta}, a_{\varepsilon}, a_{\eta} - a_{\varepsilon}.
        \]
        Our claim now reduces to the identity
        \[
            \psi(a_{\eta}) \cdot \psi^{-1}(a_{\varepsilon}) = \psi(a_{\eta} - a_{\varepsilon}).
        \]
        Next we note that we have pullback squares
        \[
        \begin{tikzcd}
            \affspace^3_{\kappa} \arrow{r}{\alpha} \arrow{d}{\pr_{13}}
            & \affspace^2_{\kappa} \arrow{d}{\pr_2}
            \\
            \affspace^2_{\kappa} \arrow{r}{\beta}
            & \affline_{\kappa}
        \end{tikzcd}
        \]
        and
        \[
            \begin{tikzcd}
                \affline_{\kappa} \arrow{r}{\pr} \arrow{d}{\Delta}
                & \Spec(\kappa) \arrow{d}{i_0}
                \\
                \affspace^2_{\kappa} \arrow{r}{\beta}
                & \affline_{\kappa}
            \end{tikzcd}
        \]
        where the map $\beta$ is given by $(x, y) \mapsto y - x$ and $i_0$ is the inclusion of the origin.
        Applying \Cref{thm:base-change} yields
        \[
            R \pr_{13, !} \circ \alpha^* \cong \beta^* \circ R \pr_{2, !} \quad \text{and} \quad \Delta_* \circ \pr^* \cong \beta^* \circ i_{0, *}.
        \]
        As a final ingredient we note that \Cref{lem:Lpsi-stalks} and \Cref{lem:fourier-stalks} together imply that $T_{\psi} (\Qlbar) \cong i_{0, *}(\Qlbar)(-1)[-1]$, i.e.\ that $R \pr_{2, !} \roundbr[\big]{m^* \calL(\psi)} \cong i_{0, *}(\Qlbar)(-1)[-2]$.
        Now putting it all together gives
        \begin{align*}
            R \pr_{13, !} \roundbr[\big]{\pr_{23}^* m^* \calL(\psi) \otimes \pr_{12}^* m^* \calL(\psi^{-1})} & \cong R \pr_{13, !} \alpha^* m^* \calL(\psi) \\
            & \cong \beta^* R \pr_{2, !} m^* \calL(\psi) \\
            & \cong \beta^* i_{0, *}(\Qlbar)(-1)[-2] \\
            & \cong \Delta_*(\Qlbar)(-1)[-2]. \qedhere
        \end{align*}
    \end{proof}

    \begin{theorem}[Fourier Inversion] \label{thm:fourier-inversion}
        For $K \in D^b_c(\affline_{\kappa})$ we have a natural isomorphism
        \[
            (T_{\psi^{-1}} \circ T_{\psi})(K) \cong K(-1).
        \]
    \end{theorem}

    \begin{proof}
        We make the following computation.
        \begin{align*}
            (T_{\psi^{-1}} \circ T_{\psi})(K)  & \cong R \pr_{1, !} \roundbr[\Big]{\pr_2^* R \pr_{1, !} \roundbr[\big]{\pr_2^* K \otimes m^* \calL(\psi)} \otimes m^* \calL(\psi^{-1})}[2] \\
            & \stackrel{\mathclap{\ref{thm:base-change}}}{\cong} R \pr_{1, !} \roundbr[\Big]{R \pr_{12, !} \pr_{2, 3}^* \roundbr[\big]{\pr_2^* K \otimes m^* \calL(\psi)} \otimes m^* \calL(\psi^{-1})}[2] \\
            & \stackrel{\mathclap{\ref{thm:proj-formula}}}{\cong} R \pr_{1, !} \roundbr[\big]{\pr_3^* K \otimes \pr_{23}^* m^* \calL(\psi) \otimes \pr_{12}^* m^* \calL(\psi^{-1})}[2] \\
            & \stackrel{\mathclap{\ref{thm:proj-formula}}}{\cong} R \pr_{1, !} \roundbr[\Big]{\pr_2^* K \otimes R \pr_{13, !} \roundbr[\big]{\pr_{23}^* m^* \calL(\psi) \otimes \pr_{12}^* m^* \calL(\psi^{-1})}}[2] \\
            & \stackrel{\mathclap{\ref{lem:key-comp}}}{\cong} R \pr_{1, !} \roundbr[\big]{\pr_2^* K \otimes \Delta_*(\Qlbar)}(-1) \\
            & \stackrel{\mathclap{\ref{thm:proj-formula}}}{\cong} R \pr_{1, !} \roundbr[\big]{\Delta_*(K)}(-1) \cong K(-1) \qedhere
        \end{align*}
    \end{proof}

    \section{End of Proof of the Main Theorem}

    Let us fix the following notation.

    \begin{itemize}
        \item
        $\psi \colon \kappa \to \Qlbar^{\times}$ is a nontrivial character as before.

        \item
        $j \colon U \to \affline_{\kappa}$ is the inclusion of a nonempty open subscheme.
        Fix a geometric point $\overline{x} \in U(k)$.

        \item
        $\calF$ is a lisse $\tau$-pure sheaf of weight $w$ on $U$.
        Write $V \coloneqq \calF_{\overline{x}}$ and let $\rho \colon W(U, \overline{x}) \to \Aut_{\Qlbar}(V)$ be the associated representation.
        We assume that the following conditions are satisfied.
        \begin{itemize}
            \item
            $\calF$ is geometrically irreducible and geometrically nonconstant.

            \item
            $\calF$ is unramified at $\infty$, i.e.\ $\calF$ extends to a lisse sheaf on $U \cup \curlybr{\infty} \subseteq \projline_{\kappa}$.
        \end{itemize}

        \item
        We also set $\calG \coloneqq j_!(\calF)$.
    \end{itemize}
    
    What remains to be shown in order to prove \Cref{thm:deligne} is the following proposition.

    \begin{proposition} \label{prop:main}
        The eigenvalues $\alpha$ of the Frobenius $\Fr \colon H^1_c(U, \calF) \to H^1_c(U, \calF)$ satisfy $\abs{\tau(\alpha)}^2 \leq q^{w + 1}$.
    \end{proposition}

    Before giving the proof of the proposition we collect some properties of the Fourier Transform $T_{\psi}(\calG)$.

    \begin{lemma} \label{lem:fourier-deg-0}
        $T_{\psi}(\calG)$ is concentrated in degree $0$.
    \end{lemma}

    \begin{proof}
        Using \Cref{lem:fourier-stalks} we need to show that
        \[
            H^i_c\roundbr[\big]{\affline_{\kappa_n}, \calG_{\kappa_n} \otimes \calL(\psi_{\kappa_n, b})} \stackrel{\ref{thm:proj-formula}}{\cong} H^i_c\roundbr[\big]{U_{\kappa_n}, \calF_{\kappa_n} \otimes j^* \calL(\psi_{\kappa_n, b})} \cong 0
        \]
        for $i = 0, 2$ and all $b \in \kappa_n$.
        For $i = 0$ this is clear because $U$ is affine of positive dimension and $\calF$ is lisse.
        For $i = 2$ we apply \Cref{thm:poincare} to see that
        \[
            H^2_c\roundbr[\big]{U_{\kappa_n}, \calF_{\kappa_n} \otimes j^* \calL(\psi_{\kappa_n, b})} \cong H^0\roundbr[\big]{U_{\kappa_n}, (\calF_{\kappa_n} \otimes j^*\calL(\psi_{\kappa_n, b}))^{\vee}}^{\vee}(-1)
        \]
        so that we need to show that the $\pi_1(U_k, \overline{x})$-invariants of $V(\psi_{\kappa_n, b})^{\vee}$ are trivial.
        $\calF$ being geometrically irreducible precisely means that $V$ is an irreducible $\pi_1(U_k, \overline{x})$-representation.
        Thus we are done if we can show that $\calF_k \not\cong \calL(\psi_{\kappa_n, b})_k$ for any $b \in \kappa_n$.
        \begin{itemize}
            \item
            As $\calF$ is geometrically nonconstant we can't have $\calF_k \cong \calL(\psi_0)_k$ (note that $\calL(\psi_0) \cong \Qlbar$).

            \item
            As $\calF$ is unramified at $\infty$ we can't have $\calF_k \cong \calL(\psi_{\kappa_n, b})_k$ for $b \neq 0$ (using that $\pi_1(\projline_k, \overline{x}) = 1$). \qedhere
        \end{itemize}
    \end{proof}

    \begin{lemma} \label{lem:fourier-mixed}
        The sheaf $T_{\psi}(\calG)$ is $\tau$-mixed.
    \end{lemma}

    \begin{proof}
        Let $b \coloneqq \tau^{-1}(q^w) \in \Qlbar^{\times}$.
        Then the sheaf
        \[
            \calH \coloneqq \roundbr[\big]{\pr_2^* j_! \calF \otimes m^* \calL(\psi)} \oplus \roundbr[\big]{\pr_2^* j_! \calF^\vee \otimes m^* \calL(\psi^{-1}) \otimes \calL_{b, \affspace^2_{\kappa}}}
        \]
        on $\affspace^2_{\kappa}$ is $\tau$-real by \Cref{lem:lin-alg} (where we use that $\calF$ is lisse and $\tau$-pure of weight $w$).
        Applying \Cref{lem:fourier-deg-0} and \Cref{thm:proj-formula} gives that
        \[
            R \pr_{1, !} \calH [1] \cong T_{\psi}(\calG) \oplus T_{\psi^{-1}}(j_! \calF^{\vee}) \otimes \calL_{b, \affline_{\kappa}}
        \]
        is concentrated in degree $0$.
        Applying \Cref{thm:base-change} and \Cref{thm:trace-formula} now shows that $R \pr_{1, !} \calH [1]$ is again $\tau$-real hence also $\tau$-mixed by \Cref{thm:real-mixed}.
        Thus also $T_{\psi}(\calG) \subseteq R \pr_{1, !} \calH [1]$ is $\tau$-mixed as desired.
    \end{proof}

    \begin{lemma} \label{lem:fourier-no-sections}
        $H^0_c(\affline_{\kappa}, T_{\psi}(\calG)) = 0$.
    \end{lemma}

    \begin{proof}
        We make the following calculation.
        \[
            H^0_c \roundbr[\big]{\affline_{\kappa}, T_{\psi}(\calG)}
            \stackrel{\mathclap{\ref{lem:fourier-stalks}}}{\cong} \scrH^{-1} \roundbr[\big]{(T_{\psi^{-1}} \circ T_{\psi})(\calG)}_0
            \stackrel{\mathclap{\ref{thm:fourier-inversion}}}{\cong} \scrH^{-1} \roundbr[\big]{\calG(-1)}_0 \cong 0 \qedhere
        \]
    \end{proof}

    \begin{proof}[Proof of \Cref{prop:main}]
        Applying the Plancherel Formula from \Cref{lem:plancherel} to $\calG$ yields
        \[
            \norm{T_{\psi} \calG} = \norm{\calG} + 1.
        \]
        Now $\calG$ and $T_{\psi}(\calG)$ are both $\tau$-mixed and we have $H^0_c(\affline_{\kappa}, \calG) \cong H^0_c(\affline_{\kappa}, T_{\psi}(\calG)) \cong 0$ (for $T_{\psi}(\calG)$ this is the content of \Cref{lem:fourier-mixed} and \Cref{lem:fourier-no-sections}) so that we can apply \Cref{thm:norm-w-curves} to obtain
        \[
            w(T_{\psi}(\calG)) = w + 1.
        \]
        This gives us precisely what we want because $T_{\psi}(\calG)_0 \cong H^1_c(\affline_{\kappa}, \calG) \cong H^1_c(U, \calF)$ by \Cref{lem:fourier-stalks}.
    \end{proof}

    \printbibliography
\end{document}