# ws2021-weil-conjectures

These are notes for a talk I am giving in the Kleine AG on the Weil conjectures in the winter term 2021/2022 (organized by Mingjia Zhang and Ben Heuer).
A compiled version can be found [here](https://makoba.gitlab.io/ws2021-weil-conjectures/fourier-theory.pdf).